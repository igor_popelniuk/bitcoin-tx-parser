#!/usr/bin/env node


// module cli
// >decode 'Bitcoin-raw-transaction'
// i.e. 'decode 01000...000'

const [,, ... args] = process.argv
const Client = require('bitcoin-core');
const client = new Client({
    network: 'testnet',
    username: 'user',
    password: 'password',
    port: 18332
});

//hex is a second argument
const hex = args[1]
const x = client.decodeRawTransaction(hex).then((response) => console.log(response));

// Work queue depth exceeded, cant proceed with developement...
//tbd vin tx_id processing to get sender adresses and amounts
